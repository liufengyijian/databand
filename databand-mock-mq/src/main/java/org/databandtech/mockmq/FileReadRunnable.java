package org.databandtech.mockmq;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class FileReadRunnable implements Runnable {

	List<String> fileStrList;
	KafkaProducer<String, String> kafkaProducer; 
	final static String TOPIC="iptvlog";
	public FileReadRunnable(KafkaProducer<String, String> kafkaProducer, List<String> str) {
		super();
		this.fileStrList = str;
		this.kafkaProducer = kafkaProducer;
	}

	@Override
	public void run() {
		long starttime = System.currentTimeMillis();
		int count = 0;
		long sumByte = 0L;
		
		for (String str : fileStrList) {
			
			//ProducerRecord<String, String> pr = new ProducerRecord<String, String>(TOPIC, str);
			//kafkaProducer.send(pr);
			System.out.println(str);
			try {
				sumByte = sumByte + str.getBytes("utf-8").length;
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			count++;
		}
		
		long endtime = System.currentTimeMillis();
		long duration = endtime - starttime;
		System.out.println(Thread.currentThread().getName() + "完成。总计时长： " + duration +",总行数： " + count+",总字节： " + sumByte);
	}

}
