package org.databandtech.clickhouse.entity;

import java.util.Map;

public class AggUserDayItem {
	
	AggUserDayCondition condition;
	Map<String , Integer> valueMap;

	public AggUserDayItem(AggUserDayCondition condition) {
		super();
		this.condition = condition;
	}
	
	public AggUserDayCondition getCondition() {
		return condition;
	}

	public void setCondition(AggUserDayCondition condition) {
		this.condition = condition;
	}
	
	public void addMap(Map<String , Integer> valueMap) {
		this.valueMap = valueMap;
	}
	
	
	public Map<String, Integer> getValueMap() {
		return valueMap;
	}
	public void setValueMap(Map<String, Integer> valueMap) {
		this.valueMap = valueMap;
	}

	@Override
	public String toString() {
		return "AggUserDayItem [condition=" + condition + ", valueMap=" + valueMap + "]";
	}
	

}
