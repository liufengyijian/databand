package org.databandtech.clickhouse;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.databandtech.clickhouse.entity.EventLog;
import org.databandtech.clickhouse.utils.Mock;

/**
 * <dependency>
    <groupId>com.github.housepower</groupId>
    <artifactId>clickhouse-native-jdbc</artifactId>
    <version>2.5.6</version>
</dependency>
 * @author 86137
 *
 */
public class MockDataNative {
	
	final static String[] CITYS = {"北京","北京","北京","上海","上海","上海","广州","广州","深圳","深圳","重庆","杭州","武汉","南京","郑州","西安","成都","长沙"};
	final static String[] APPS = {"jg","mtv"};
	final static int COUNT=100000;    //发送的数据条数
	final static int BATCHCOUNT=1500;    //批量条数
	final static int PARTITION=0; //分区
	final static String DATE="2021-09-18 "; //日志日期
	final static int MAXUUSERID=10000; //Uid范围
	final static int MAXVID=3000; //Vid范围
	
	public static void main(String[] args) throws SQLException {
		try (Connection connection = DriverManager.getConnection("jdbc:clickhouse://192.168.13.115:8123")) {
            try (Statement stmt = connection.createStatement()) {

                long starttime = System.currentTimeMillis();
                try (PreparedStatement pstmt = connection.prepareStatement("INSERT INTO SNM_initial20210916(EventDate,Uid,Vid,Source,Logtype,City,Duration) VALUES (?,?,?,?,?,?,?)")) {
                	int index = 0 ;
                	for (int i = 1; i <= COUNT; i++) {
                    
                		index++;
                    	//String eventTime = DATE+Mock.getNum(10, 23)+":"+Mock.getNum(10, 30)+":00";
                    	String eventTime = DATE+Mock.getNum(10, 23)+":"+Mock.getNum(10, 30)+":00";
                    	
        				EventLog log = new EventLog(eventTime, 
        						Mock.getNum(0, MAXUUSERID), Mock.getNum(0, MAXVID)+"", APPS[Mock.getNum(0, APPS.length-1)], "vh", CITYS[Mock.getNum(0, CITYS.length-1)], Mock.getNum(0, 500));
        				
        				DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        				LocalDateTime localDateTime = LocalDateTime.parse(log.getEventtime(),df);
        				Long longtimestamp = localDateTime.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
        				
        				//pstmt.setDate(1, new Date(System.currentTimeMillis()));
        				pstmt.setDate(1, new Date(longtimestamp));
        				pstmt.setInt(2,log.getUid());
        				pstmt.setString(3, log.getVid());
        				pstmt.setString(4, log.getSource());
        				pstmt.setString(5, log.getLogtype());
        				pstmt.setString(6, log.getCity());
        				pstmt.setInt(7,log.getDuration());

        				//单条执行非常慢，弃用
        				//pstmt.execute();
        				
                        pstmt.addBatch();
                        
                        if (index == BATCHCOUNT) {
                        	pstmt.executeBatch();
                        	index = 0;
                        }
                    }  
                }
                
        		long endtime = System.currentTimeMillis();
        		long duration = endtime - starttime;
        		System.out.println("插入"+COUNT+"记录的时长： " + duration);

        		String sql = "select count(*) from SNM_initial20210916";
                try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
                    printCount(pstmt,sql);
                }

            }
        }
    }

    public static void printCount(PreparedStatement pstmt,String sql) throws SQLException {
        try (ResultSet rs = pstmt.executeQuery()) {
            System.out.println(LocalDateTime.now()  );
            System.out.println(  sql);
            if (rs.next())
                System.out.println(rs.getInt(1));
        }
    }

}
