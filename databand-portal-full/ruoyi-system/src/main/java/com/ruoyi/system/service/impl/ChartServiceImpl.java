package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.domain.DyDataset;
import com.ruoyi.system.mapper.ChartMapper;
import com.ruoyi.system.service.IChartService;

@Service
public class ChartServiceImpl implements IChartService {
	
    @Autowired
    private ChartMapper chartMapper;

	@Override
	public List<DyDataset> selectDatasetList(String datetype, String productline, String channel) {

		return chartMapper.selectDatasetList(datetype, productline, channel);
	}
    
    

}
