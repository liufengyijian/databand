import request from '@/utils/request'

export function getAllSite() {
  return request({
    url: '/sites/list',
    method: 'get'
  })
}

export function getAllSiteByUsername(username) {
  return request({
    url: '/sites/listbyusername',
    method: 'get',
    params: { username }
  })
}

export function getSiteMenuTree() {
  return request({
    url: '/sites/tree',
    method: 'get'
  })
}

