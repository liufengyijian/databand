package org.databandtech.streamjob.jobs;

import java.util.Properties;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.databandtech.streamjob.sink.SinkToClickhouse;

public class KafkaToClickHouseJob implements Runnable{

	final static String READ_TOPIC = "Hello-Kafka";
	
	// Clickhouse 相关
	final static String CHURL = "jdbc:clickhouse://localhost:8123/databand";
	final static String CHUSER = "root";
	final static String CHPASS = "mysql";
	final static String CHSQL = "INSERT INTO test.writer";

	@Override
	public void run() {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.enableCheckpointing(5000); // 每隔 5000 毫秒 执行一次 checkpoint
		FlinkKafkaConsumer<String> kafkaConsumer = readStreamData();
		sinkData(env, kafkaConsumer);	
	}
	
	private void sinkData(StreamExecutionEnvironment env, FlinkKafkaConsumer<String> kafkaConsumer) {
		DataStream<String> streamInput = env
				  .addSource(kafkaConsumer);
				
		streamInput.print();
		streamInput.addSink(new SinkToClickhouse(CHURL,CHUSER,CHPASS,CHSQL));		
	}

	private FlinkKafkaConsumer<String> readStreamData() {
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "192.168.10.60:9092");
		properties.setProperty("group.id", "test");
		properties.setProperty("stream.parallelism", "4");
		//数据读取
		FlinkKafkaConsumer<String> kafkaConsumer = new FlinkKafkaConsumer<>(READ_TOPIC, new SimpleStringSchema(), properties);
		
		// 从最早的记录开始,全量采集
		kafkaConsumer.setStartFromEarliest();     
		//kafkaConsumer.setStartFromLatest();       // 从最新的记录开始
		//kafkaConsumer.setStartFromTimestamp(startupOffsetsTimestamp); // 从指定的时间开始（毫秒）
		//kafkaConsumer.setStartFromSpecificOffsets(specificStartupOffsets);
		//从指定分区的位置开始
		//Map<KafkaTopicPartition, Long> specificStartOffsets = new HashMap<>();
		//specificStartOffsets.put(new KafkaTopicPartition("myTopic", 0), 23L);
		//specificStartOffsets.put(new KafkaTopicPartition("myTopic", 1), 31L);
		//specificStartOffsets.put(new KafkaTopicPartition("myTopic", 2), 43L);
		//kafkaConsumer.setStartFromSpecificOffsets(specificStartOffsets);
		
		// 默认的方法
		//kafkaConsumer.setStartFromGroupOffsets(); 
		
		return kafkaConsumer;
	}

}
