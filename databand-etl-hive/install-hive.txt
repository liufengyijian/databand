cd /usr/app
wget https://mirror.bit.edu.cn/apache/hive/stable-2/apache-hive-2.3.7-bin.tar.gz
tar -zxvf apache-hive-2.3.7-bin.tar.gz

安装时注意HDFS文件系统的临时目录要可写

hadoop dfsadmin -safemode leave
hadoop fs -chmod -R 777 /tmp
hadoop fs -chmod -R 777 /user/hive/warehouse

配置环境
vi /etc/profile

export HIVE_HOME=/usr/app/apache-hive-2.3.7-bin
export PATH=$HIVE_HOME/bin:$PATH

$ $HADOOP_HOME/bin/hadoop fs -mkdir       /tmp
$ $HADOOP_HOME/bin/hadoop fs -mkdir       /user/hive/warehouse
$ $HADOOP_HOME/bin/hadoop fs -chmod g+w   /tmp
$ $HADOOP_HOME/bin/hadoop fs -chmod g+w   /user/hive/warehouse

chmod g+w XXX

修改配置hive-env.sh

$ cd $HIVE_HOME/conf
$ cp hive-env.sh.template hive-env.sh
vi hive-env.sh
配置里加入hadoop路径

需要下载mysql的jdbc<mysql-connector-java-5.1.28.jar>，然后将下载后的jdbc放到hive安装包的lib目录下， 下载链接是：http://dev.mysql.com/downloads/connector/j/ 

修改hive-site.xml文件
cp hive-default.xml.template hive-site.xml
vi hive-site.xml

        <!-- 插入一下代码 记得删除原有的节点-->
    <property>
        <name>javax.jdo.option.ConnectionUserName</name>
        <value>root</value>
    </property>
    <property>
        <name>javax.jdo.option.ConnectionPassword</name>
        <value>mysql</value>
    </property>
   <property>
        <name>javax.jdo.option.ConnectionURL</name>
        <value>jdbc:mysql://192.168.13.66:3307/hive?useSSL=false</value>
    </property>
    <property>
        <name>javax.jdo.option.ConnectionDriverName</name>
        <value>com.mysql.jdbc.Driver</value>
    </property>
      <property>
    <name>datanucleus.schema.autoCreateAll</name>
    <value>true</value>
    <description>Auto creates necessary schema on a startup if one doesn't exist. Set this to false, after creating it once.To enable auto create also set hive.metastore.schema.verification=false. Auto creation is not recommended for production use cases, run schematool command instead.</description>
  </property>
  <property>
    <name>hive.metastore.schema.verification</name>
    <value>false</value>
    <description>
      Enforce metastore schema version consistency.
      True: Verify that version information stored in is compatible with one from Hive jars.  Also disable automatic
            schema migration attempt. Users are required to manually migrate schema after Hive upgrade which ensures
            proper metastore schema migration. (Default)
      False: Warn if the version information stored in metastore doesn't match with one from in Hive jars.
    </description>
  </property>
        <!-- 到此结束代码 -->

在mysql中新加hive的schema（在此之前需要创建mysql下的hive数据库）

cd /usr/app/apache-hive-2.3.7-bin/bin
schematool -dbType mysql -initSchema

解决URISyntaxException: Relative path in absolute URI: ${system:java.io.tmpdir

1.查看hive-site.xml配置，会看到配置值含有"system:java.io.tmpdir"的配置项
2.新建文件夹/home/grid/hive-0.14.0-bin/iotmp
3.将含有"system:java.io.tmpdir"的配置项的值修改为如上地址

mysql执行

grant all privileges  on *.* to root@'%' identified by "mysql";
flush privileges;
select host,user,password from user;

或者
update user set host = '%' where user = 'root';