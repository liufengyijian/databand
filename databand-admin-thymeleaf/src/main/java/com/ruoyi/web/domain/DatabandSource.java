package com.ruoyi.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据源对象 databand_source
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 数据源标题 */
    @Excel(name = "数据源标题")
    private String title;

    /** 驱动 */
    @Excel(name = "驱动")
    private String drive;

    /** 数据源连接 */
    @Excel(name = "数据源连接")
    private String conn;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDrive(String drive) 
    {
        this.drive = drive;
    }

    public String getDrive() 
    {
        return drive;
    }
    public void setConn(String conn) 
    {
        this.conn = conn;
    }

    public String getConn() 
    {
        return conn;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("drive", getDrive())
            .append("conn", getConn())
            .append("username", getUsername())
            .append("password", getPassword())
            .toString();
    }
}
