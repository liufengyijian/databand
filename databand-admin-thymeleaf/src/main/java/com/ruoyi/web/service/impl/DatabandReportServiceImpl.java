package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandReportMapper;
import com.ruoyi.web.domain.DatabandReport;
import com.ruoyi.web.service.IDatabandReportService;
import com.ruoyi.common.core.text.Convert;

/**
 * 报Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandReportServiceImpl implements IDatabandReportService 
{
    @Autowired
    private DatabandReportMapper databandReportMapper;

    /**
     * 查询报
     * 
     * @param id 报ID
     * @return 报
     */
    @Override
    public DatabandReport selectDatabandReportById(Long id)
    {
        return databandReportMapper.selectDatabandReportById(id);
    }

    /**
     * 查询报列表
     * 
     * @param databandReport 报
     * @return 报
     */
    @Override
    public List<DatabandReport> selectDatabandReportList(DatabandReport databandReport)
    {
        return databandReportMapper.selectDatabandReportList(databandReport);
    }

    /**
     * 新增报
     * 
     * @param databandReport 报
     * @return 结果
     */
    @Override
    public int insertDatabandReport(DatabandReport databandReport)
    {
        return databandReportMapper.insertDatabandReport(databandReport);
    }

    /**
     * 修改报
     * 
     * @param databandReport 报
     * @return 结果
     */
    @Override
    public int updateDatabandReport(DatabandReport databandReport)
    {
        return databandReportMapper.updateDatabandReport(databandReport);
    }

    /**
     * 删除报对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReportByIds(String ids)
    {
        return databandReportMapper.deleteDatabandReportByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报信息
     * 
     * @param id 报ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReportById(Long id)
    {
        return databandReportMapper.deleteDatabandReportById(id);
    }

	@Override
	public List<DatabandReport> selectDatabandReportList() {
		return databandReportMapper.selectDatabandReportListAll();
	}
}
